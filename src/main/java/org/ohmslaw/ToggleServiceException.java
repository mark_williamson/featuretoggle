package org.ohmslaw;

public class ToggleServiceException extends Exception {

    public ToggleServiceException(String message) {
        super(message);
    }
}
