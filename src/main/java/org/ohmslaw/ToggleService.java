package org.ohmslaw;

import java.util.HashMap;
import java.util.Map;

public class ToggleService {

    private Map<String,Boolean> toggleMap = new HashMap();

    private static ToggleService toggleService;

    public static ToggleService getInstance() {
         if (toggleService==null) {
             toggleService = new ToggleService();
         }
         return toggleService;
    }

    public boolean isToggleEnabled(String toggleName) throws ToggleServiceException {
        if (toggleName==null) {
            throw new ToggleServiceException("Toggle name may not be null");
        }
        if (! toggleMap.containsKey(toggleName.toLowerCase())) {
            throw new ToggleServiceException("Toggle does not exist");
        }
        return toggleMap.get(toggleName.toLowerCase());
    }

    public void createToggle(String toggleName, boolean initalState) throws ToggleServiceException {
        if (toggleMap.containsKey(toggleName.toLowerCase())) {
            throw new ToggleServiceException("Toggle already exists");
        }
        toggleMap.put(toggleName.toLowerCase(),initalState);
    }

    public void updateToggle(String toggleName, boolean newState) throws ToggleServiceException {
        if (! toggleMap.containsKey(toggleName.toLowerCase())) {
            throw new ToggleServiceException("Toggle does not exist");
        }
        toggleMap.put(toggleName.toLowerCase(),newState);
    }

    public void deleteToggle(String toggleName) throws ToggleServiceException {
        if (! toggleMap.containsKey(toggleName.toLowerCase())) {
            throw new ToggleServiceException("Toggle does not exist");
        }
        toggleMap.remove(toggleName.toLowerCase());
    }
}
