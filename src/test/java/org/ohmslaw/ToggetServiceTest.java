package org.ohmslaw;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.IsEqual.equalTo;
import static org.junit.Assert.assertEquals;

public class ToggetServiceTest {

    private ToggleService toggleService;
    @Before
    public void setup() {
        toggleService = ToggleService.getInstance();
    }

    @Test
    public void doesGetInstanceReturnSameSingleton() {
        ToggleService toggleService1 = ToggleService.getInstance();
        ToggleService toggleService2 = ToggleService.getInstance();

        assertEquals(toggleService1,toggleService2);
    }

    @Test
    public void canToggleServiceCreateAndReadAToggle() throws ToggleServiceException {
        ToggleService toggleService = ToggleService.getInstance();

        toggleService.createToggle("TestToggle",true);

        boolean result = toggleService.isToggleEnabled("TestToggle");

        assertThat(result,equalTo(true));

    }

    @Test
    public void canToggleBeUpdated() throws ToggleServiceException {
        toggleService.createToggle("NewToggle",true);
        toggleService.updateToggle("NewToggle",false);

        boolean result = toggleService.isToggleEnabled("NewToggle");
        assertThat(result,equalTo(false));

    }



    @Rule
    public final ExpectedException thrown = ExpectedException.none();

    @Test
    public void canToggleBeDeleted()  throws ToggleServiceException {
        thrown.expect(ToggleServiceException.class);
        thrown.expectMessage("Toggle does not exist");
        toggleService.createToggle("NewToggle",true);
        toggleService.deleteToggle("NewToggle");
        toggleService.isToggleEnabled("NewToggle");
    }

    @Test
    public void isTheToggleServiceNullSafe()  throws ToggleServiceException {
        thrown.expect(ToggleServiceException.class);
        thrown.expectMessage("Toggle name may not be null");
        toggleService.isToggleEnabled(null);
    }

}
